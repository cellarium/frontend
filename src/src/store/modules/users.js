import Vue from "vue";

const state = {
  user: null,
};

const getters = {
  isAuthenticated: (state) => !!state.user,
  getUser: (state) => state.user,
};

const actions = {
  logout({ commit }) {
    let user = null;
    localStorage.removeItem("token");
    delete Vue.prototype.$http.defaults.headers.common["Authorization"];
    commit("logout", user);
  },
};

const mutations = {
  setUser(state, username) {
    state.user = username;
  },
  logout(state, user) {
    state.user = user;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
