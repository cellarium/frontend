# Builder
FROM node:alpine As builder

RUN npm install -g @vue/cli

WORKDIR /app

COPY ./src .
COPY .env .

RUN npm install

RUN npx vue-cli-service lint

RUN npx vue-cli-service build


# Final
FROM nginx:alpine

COPY --from=builder /app/dist /usr/share/nginx/html
